from django.urls import path
from . import views
from .views import *
from .views import formulir_donatur

app_name = 'main'

urlpatterns = [
    path('', views.formulir_donatur, name='formulir_donatur'),
    path('terimakasih', views.terimakasih, name = 'terimakasih')
]
