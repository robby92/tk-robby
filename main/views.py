from django.shortcuts import render
from django.shortcuts import render, redirect
from .models import*
from .forms import*


def terimakasih(request):
    return render(request, 'terimakasih.html')


def formulir_donatur(request):   
    if(request.method == "POST"):
        form = formulir_payment(request.POST)
        if(form.is_valid()):
            form2 = Payment()
            form2.Nominal = form.cleaned_data["Nominal"]
            form2.Nama = form.cleaned_data["Nama"]
            form2.Email = form.cleaned_data["Email"]
            form2.save()    
        return redirect("/terimakasih")
    else:
        form =  formulir_payment()
        form2 = Payment.objects.all()
        form_dictio = {
            'formulir' : form,
            'formulir_donatur' : form2
        }
        return render(request, 'base.html', form_dictio)