# Generated by Django 3.1.2 on 2020-10-26 05:31

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Nominal', models.IntegerField(max_length=100)),
                ('Nama', models.CharField(max_length=100)),
                ('Email', models.CharField(max_length=100)),
            ],
        ),
    ]
