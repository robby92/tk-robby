from django.db import models

# Create your models here.
class Payment(models.Model):
    Nominal = models.IntegerField(blank=False)
    Nama = models.CharField(blank=False, max_length= 100)
    Email = models.CharField(blank=False, max_length= 100)  